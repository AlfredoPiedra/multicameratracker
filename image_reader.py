from cv2 import imread
from sys import exit
from linecache import getline
from constants import DIRECTORY_PATH
from abstract_reader import AbstractReader

class ImageReader(AbstractReader):

    def __init__(self,
                 full_image_list_path):
        
        try:

            file = open(full_image_list_path,"r")

            total_frames = len(file.readlines())
            
            file.close() 

            super().__init__(full_source_path=full_image_list_path,
                             source_total_elements=total_frames,
                             initial_counter_value=1)
        except IOError:

            print("[ImageReader][init]: could not open or read the image list file")
            exit()
        
    def get_next_frame(self):

        if self._reader_counter <= self._source_length: 

            current_file = getline(self._full_source_path,
                                   self._reader_counter)
        
            current_file = current_file.replace("\n","") + ".png"
        
            image = imread(DIRECTORY_PATH
                           + "/multimedia/"
                           + current_file)

            if image is None or image.size == 0:

                print("[ImageReader][get_next_frame]: image file = "+current_file+" doesn't exist")
                return False,None,0,0

            self._reader_counter += 1
            
            height = image.shape[0]
            width = image.shape[1]
            
            return True,image,height,width

        print("[ImageReader][get_next_frame]: frame source end reached, there is no frame left to read")
        return False,None,0,0

    def get_frame_at(self,
                     index):

        if ( (index <= 0) or 
             (index > self._source_length)):

            print("[ImageReader][get_frame_at]: invalid index value: "+str(index))
            return False,None,0,0

        current_file = getline(self._full_source_path, index)
        current_file = current_file.replace("\n","") + ".png"

        image = imread(DIRECTORY_PATH
                       + "/multimedia/"
                       + current_file)

        if image is None or image.size == 0:

            print("[ImageReader][get_frame_at]: could not read image with index "+str(index))
            return False,None,0,0

        self._reader_counter = index+1

        height = image.shape[0]
        width = image.shape[1]
    
        return True,image,height,width