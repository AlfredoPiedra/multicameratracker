import abc

'''
Abstract Reader Class

Base class for the multimedia reading abstraction layer 

'''

class AbstractReader:

    # Inputs: 
    #           full_source_path: string = absolute path        
    #           source_total_elements: int = total number of frames in the multimedia file
    #           initial_counter_value: int = frame counter initial value           
    def __init__(self,
                 full_source_path,
                 source_total_elements,
                 initial_counter_value):

        # Absolute path of the multimedia, which can be a video file or 
        # a text file with a list of image filenames.
        self._full_source_path = full_source_path

        # The total number of frames of the multimedia
        self._source_length = source_total_elements

        # Secuential counter used to access one frame at the time
        self._reader_counter = initial_counter_value

    # Inputs: 
    #           counter_value: int = new value for the frame counter
    def set_reader_counter(self, 
                           counter_value):
    
        # Check for the limits, before increment the counter
        # This is helpful because indicates when all the 
        # frames of the source has been analyzed.
        if ((counter_value > 0) or 
            (counter_value <= self._source_length)):

            self._reader_counter = counter_value

        else:

            print("[AbstractReader][set_reader_counter]: invalid counter value: " 
                  + str(counter_value)
                  + " must be between [1," 
                  + str(self._source_length) 
                  + "]")
            exit()

    def get_source_length(self):

        return self._source_length

    def get_full_source_path(self):

        return self._full_source_path

    def get_reader_counter(self):

        return self._reader_counter

    @abc.abstractclassmethod
    def get_next_frame(self):
        pass

    @abc.abstractclassmethod
    def get_frame_at(self,
                     index):
        pass
