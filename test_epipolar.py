import cv2
import numpy as np
from constants import DIRECTORY_PATH
from epipolar_constraint import EpipolarConstraint


def draw_epipolar_line(imageL_name,
                       imageR_name,
                       pixel_point,
                       constraint):

    imagR = cv2.imread(DIRECTORY_PATH
                       + "/multimedia/"
                       + imageR_name)
    
    imagL = cv2.imread(DIRECTORY_PATH
                       + "/multimedia/"
                       + imageL_name)

    lineas = constraint.get_left_camera_epipolar_lines(pixel_point)

    a1 = lineas[0][0]
    a2 = lineas[0][1]
    a3 = lineas[0][2]

    xa = 0.0
    ya = -a3 / a2

    xb = 1280.0
    yb = -(a1 * xb + a3) / a2

    xa,ya = map(int,[xa,ya])
    xb,yb = map(int,[xb,yb])
    
    cv2.circle(imagR,(pixel_point[0][0],pixel_point[0][1]),2,(0,255,0),1)
    cv2.line(imagL,(xa,ya),(xb,yb),(0,255,0),1)
    
    cv2.imshow("Left image",imagL)
    cv2.imshow("Right image",imagR)
    cv2.waitKey(0)
    cv2.destroyAllWindows()   

def main():

    stereo = EpipolarConstraint('C','B')

    draw_epipolar_line("7-0001.png",
                       "8-0001.png",
                       #np.array([np.array([983,467,1])]),
                       #np.array([np.array([463,475,1])]),
                       np.array([np.array([862,551,1])]),
                       stereo)
    
if __name__ == "__main__":

    main()