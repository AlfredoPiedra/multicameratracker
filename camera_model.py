import sys
import numpy as np
from constants import CAMERA_IDS,DIRECTORY_PATH

'''
Camera Model Class

Loads both the intrinsic and extrinsic 
calibration matrices of a specific camera

'''

class CameraModel:


    # Inputs: 
    #         camera: char = used as the camera id, must be a capital letter of the alphabet
    # 
    # Outputs:
    # 
    #        K,R: float[][] = matrix with dimension 3x3
    #        t: float[] = vector with dimension 1x3        
    def __init__(self,
                camera):

        self.__K,self.__R,self.__t = self.__get_camera_matrices(camera)

    # Inputs: 
    #         camera_id: char = used as the camera id, must be a capital letter of the alphabet
    # 
    # Outputs:
    # 
    #        K,R: float[][] = matrix with dimension 3x3
    #        t: float[] = vector with dimension 1x3
    def __get_camera_matrices(self,
                              camera_id):

        # Make sure the specified id for the camera is valid
        if camera_id not in CAMERA_IDS:

            print("[CameraModel]: unknown camera id specified")
            sys.exit()

        # Load the corresponding [R|t] matrix
        Rt = np.loadtxt(DIRECTORY_PATH  + 
                        "/multimedia/cam"+
                        str(camera_id)  +
                        "_RT.txt")

        # Load the corresponding intrinsic calibration matrix
        K =  np.loadtxt(DIRECTORY_PATH  + 
                        "/multimedia/cam"+
                        str(camera_id)  +
                        "_K.txt")

        # Get the rotation matrix from the [R|t] matrix
        R = np.array([
                        [Rt[0][0], Rt[0][1], Rt[0][2]],
                        [Rt[1][0], Rt[1][1], Rt[1][2]],
                        [Rt[2][0], Rt[2][1], Rt[2][2]]
                    ])
        
        # Get the traslation vector from the [R|t] matrix
        t = np.array([
                        Rt[0][3],
                        Rt[1][3],
                        Rt[2][3]
                    ])

        return K,R,t

    def get_K(self):
        return self.__K

    def get_R(self):
        return self.__R

    def get_t(self):
        return self.__t

    