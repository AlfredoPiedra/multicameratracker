from sys import exit
from json import loads
from constants import OUTPUT_FORMATS

class InputFormat:

    def __init__(self,
                 format_option=None):
        
        # Used to know in which format are the 
        # coordinates specified in the JSON Objects
        self.__format_mode = format_option

    def __load_json_objects(self,
                            filename):

        # List for save the dictionaries
        dict_list = []

        try:

            # Open / close the input file
            with open(filename,'r') as document:

                for line in document:

                    # NOTE: Uncomment the next line if the input file 
                    # was not created by a method of the class OutputFormat 
                    # line = line.replace("'", "\"")

                    # Load the JSON object and convert to a Python dictionary
                    dictionary = loads(line)

                    # Add the dictionary to the result list
                    dict_list.append(dictionary)
            
            return dict_list
        
        except IOError:

            print("[InputFormat][read_results]: could not open or read the file: " + filename)
            exit()

    def __convert_from_yolo_to_opencv(self,
                                      detections_dict):

        return detections_dict

    def read_results(self,
                     filename):

        # Read the JSON Objects and convert to Python dictionaries
        dictionary_list = self.__load_json_objects(filename=filename)

        # In case we want to change the coordinates from the opencv format to the YOLO format
        if self.__format_mode == OUTPUT_FORMATS['yolo']:

            # Get the equivalent coordinates in the YOLO format
            yolo_format_detections = self.__convert_from_yolo_to_opencv(dictionary_list)
            
            # Return a list of dictionaries with the coordinates values in YOLO format
            return yolo_format_detections


        # In case a change of the coordinates format is not needed
        # just return a list of dictionaries with the coordinates
        # values in the opencv format
        return dictionary_list




            





    
