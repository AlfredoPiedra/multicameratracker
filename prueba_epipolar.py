from os import path
import cv2

DIRECTORY_PATH = path.dirname(path.realpath(__file__))

frame = cv2.imread(DIRECTORY_PATH + "/multimedia/2-0001.png")

def view(points,line):

    for point in points:

        cv2.circle(frame,point,1,(0,0,255),2)


    a = line[0]
    b = line[1]
    c = line[2]

    x1 = 0
    x2 = 1200

    y1 = int((-a*x1-c) / b)
    y2 = int((-a*x2-c) / b)
    


    cv2.line(frame,(x1,y1),(x2,y2),(0,255,0),2)

    cv2.imshow("Frame",frame)
    cv2.waitKey(0)

    cv2.destroyAllWindows()

if __name__ == '__main__':

    points = [(714, 346), (724, 519), (878, 637)]
    #line = [-0.11009799, -0.07522648, 54.09456512]
    #line = [-0.06856077,  0.08099382, 27.4488777 ]
    line = [0.07279116, 0.20949013, -43.55882735]
    view(points,line)
