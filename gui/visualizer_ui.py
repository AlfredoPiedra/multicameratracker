from PyQt4 import QtCore, QtGui
from PyQt4.phonon import Phonon

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):

    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(787, 600)
        
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        
        self.horizontalLayoutA = QtGui.QHBoxLayout()
        self.horizontalLayoutA.setObjectName(_fromUtf8("horizontalLayoutA"))
        
        self.horizontalLayoutB = QtGui.QHBoxLayout()
        self.horizontalLayoutB.setObjectName(_fromUtf8("horizontalLayoutB"))

        self.horizontalLayoutC = QtGui.QHBoxLayout()
        self.horizontalLayoutC.setObjectName(_fromUtf8("horizontalLayoutC"))
        
        self.verticalLayout.addLayout(self.horizontalLayoutA)
        self.verticalLayout.addLayout(self.horizontalLayoutB)
        self.verticalLayout.addLayout(self.horizontalLayoutC)

        MainWindow.setCentralWidget(self.centralwidget)
        
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 787, 25))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuFile = QtGui.QMenu(self.menubar)
        self.menuFile.setObjectName(_fromUtf8("menuFile"))
        MainWindow.setMenuBar(self.menubar)
        
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        
        self.actionLoad = QtGui.QAction(MainWindow)
        self.actionLoad.setObjectName(_fromUtf8("actionLoad"))
        self.actionLoad.triggered.connect(self.File_Selector)
        self.menuFile.addAction(self.actionLoad)
        self.menubar.addAction(self.menuFile.menuAction())

        VIDEO_PATH = 'trackingResults/BFinal.avi'
        VIDEO2_PATH = 'trackingResults/CFinal.avi'

        self.video_playerA = Phonon.VideoPlayer(Phonon.VideoCategory, self.centralwidget)
        self.video_playerB = Phonon.VideoPlayer(Phonon.VideoCategory, self.centralwidget)
        self.video_playerC = Phonon.VideoPlayer(Phonon.VideoCategory, self.centralwidget)
        
        self.play_button = QtGui.QPushButton("Pausa", self.centralwidget)
        self.stop_button = QtGui.QPushButton("Detener", self.centralwidget)

        # Conectar los eventos con sus correspondientes funciones.
        self.play_button.clicked.connect(self.play_clicked)
        self.stop_button.clicked.connect(self.stop_clicked)
        self.video_playerA.mediaObject().stateChanged.connect(
            self.state_changed)
        self.video_playerB.mediaObject().stateChanged.connect(
            self.state_changed)
        self.video_playerC.mediaObject().stateChanged.connect(
            self.state_changed)
        
        self.seek_slider = Phonon.SeekSlider(self.centralwidget)
        self.seek_sliderB = Phonon.SeekSlider(self.centralwidget) 
        self.seek_slider.setMediaObject(self.video_playerA.mediaObject())
        self.seek_sliderB.setMediaObject(self.video_playerB.mediaObject())
        #self.seek_slider.setMediaObject(self.video_playerC.mediaObject())

        self.horizontalLayoutA.addWidget(self.video_playerA)
        self.horizontalLayoutA.addWidget(self.video_playerB)
        #self.horizontalLayoutB.addWidget(self.video_playerC)
        self.horizontalLayoutC.addWidget(self.play_button, QtCore.Qt.AlignHCenter)
        self.horizontalLayoutC.addWidget(self.stop_button, QtCore.Qt.AlignHCenter)
        self.verticalLayout.addWidget(self.seek_slider, QtCore.Qt.AlignHCenter)
        self.verticalLayout.addWidget(self.seek_sliderB, QtCore.Qt.AlignHCenter)

        self.video_playerA.play(Phonon.MediaSource(VIDEO_PATH))
        self.video_playerB.play(Phonon.MediaSource(VIDEO2_PATH))
        #self.video_playerC.play(Phonon.MediaSource(VIDEO_PATH))

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.menuFile.setTitle(_translate("MainWindow", "File", None))
        self.actionLoad.setText(_translate("MainWindow", "Load", None))

    def File_Selector(self):
        filenames = QtGui.QFileDialog.getOpenFileNames(self.centralwidget, "Select File", "", "*.txt")
        print(filenames)
        for filename in filenames:
            text = open(filename).read()
            #print(text)

    def play_clicked(self):
        """
        Comenzar o resumir la reproducción.
        """
        if (self.video_playerA.mediaObject().state() in
            (Phonon.PausedState, Phonon.StoppedState)):
            self.video_playerA.play()
        else:
            self.video_playerA.pause()

        if (self.video_playerB.mediaObject().state() in
            (Phonon.PausedState, Phonon.StoppedState)):
            self.video_playerB.play()
        else:
            self.video_playerB.pause()

        if (self.video_playerC.mediaObject().state() in
            (Phonon.PausedState, Phonon.StoppedState)):
            self.video_playerC.play()
        else:
            self.video_playerC.pause()
    
    def stop_clicked(self):
        """
        Detener la reproducción.
        """
        self.video_playerA.stop()
        self.video_playerB.stop()
        self.video_playerC.stop()
    
    def state_changed(self, newstate, oldstate):
        """
        Actualizar el texto de los botones de reproducción y pausa.
        """
        states = {
            Phonon.PausedState: "Resumir",
            Phonon.PlayingState: "Pausa",
            Phonon.StoppedState: "Reproducir"
        }
        self.play_button.setText(states[newstate])
        self.stop_button.setEnabled(newstate != Phonon.StoppedState)


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

