import cv2
import sys
from abstract_reader import AbstractReader

class VideoReader(AbstractReader):

    def __init__(self,
                 full_video_path):

        self.__video = cv2.VideoCapture(full_video_path)

        if not self.__video.isOpened():

            print("[VideoReader][init]: could not open the video file")
            sys.exit()

        super().__init__(full_source_path=full_video_path,
                         source_total_elements=int(self.__video.get(cv2.CAP_PROP_FRAME_COUNT)),
                         initial_counter_value=1)

    def get_next_frame(self):

        if self._reader_counter <= self._source_length:

            ok,frame = self.__video.read()

            if not ok:

                print("[VideoReader][get_next_frame]: could not read the next frame from video file")
                return False,None,0,0

            self._reader_counter += 1

            width  = int(self.__video.get(cv2.CAP_PROP_FRAME_WIDTH))
            height = int(self.__video.get(cv2.CAP_PROP_FRAME_HEIGHT))

            return True,frame,height,width

        print("[VideoReader][get_next_frame]: frame source end reached, there is no frame left to read")
        return False,None,0,0

    def get_frame_at(self,
                     index):

        if ( (index <= 0) or 
             (index > self._source_length)):

            print("[VideoReader][get_frame_at]: invalid index value: "+str(index))
            return False,None,0,0

        self.__video.set(cv2.CAP_PROP_POS_FRAMES, index-1)

        ok,frame = self.__video.read()

        if not ok:

            print('[VideoReader][get_frame_at]: could not read frame at index: '+str(index))
            return False,None,0,0

        self._reader_counter = index+1

        width  = int(self.__video.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(self.__video.get(cv2.CAP_PROP_FRAME_HEIGHT))

        return True,frame,height,width