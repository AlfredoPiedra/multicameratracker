
class Track:
    
    def __init__( self,
                  init_track_id,
                  init_detection):

        # Track ID
        self.track_id = init_track_id 

        # Last detection appended to track
        self.last_detection = init_detection

        # Initial track score
        self.track_score = 0.001

        # Primary trackers lost flag
        self.trackers_lost = False

    def get_track_id(self):
        return self.track_id

    def get_last_detection(self):
        return self.last_detection

    def set_last_detection(self,
                           new_detection):

        self.last_detection = new_detection

    def get_track_score(self):
        return self.track_score
    
    def are_trackers_lost(self):

        return self.trackers_lost
    
    def set_lost_trackers(self, value):

        self.trackers_lost = value

    def set_track_score(self,
                        new_score):

        self.track_score = new_score

    def update( self,
                detection,
                score,
                trackers_lost):

        # Extend hypothesis with a new observation

        # Extended with a dummy observation
        if detection is None:
            
            self.track_score  += 0.001
            
            self.trackers_lost = True

        else:

            self.track_score += score

            self.last_detection = detection

            self.trackers_lost = trackers_lost
            
    def __str__(self):

        all_info = "(Track ID: "
        all_info += str(self.track_id)
        all_info += ", Last Detection: "
        all_info += str(self.last_detection)
        all_info += ", Track_Score: "
        all_info += str(self.track_score)
        all_info += ", Lost_Trackers: "
        all_info += str(self.trackers_lost)
        all_info += ")"

        return all_info