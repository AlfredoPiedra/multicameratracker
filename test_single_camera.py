import time

from singlecamera_tracker import SingleCameraTracker

def main():

    tracker = SingleCameraTracker(media_source="image",
                                  media_filename="camB_images_list.txt",
                                  tracker_id='B',
                                  save_frecuency=1)

    start = time.time()
    
    for v in range(50):

        print('Frame: '+str(v))
        tracker.run_on_next_frame()

    #tracker.run_continuosly()

    total = time.time() - start

    print("Execution time: " + str(total))

if __name__ == '__main__':

    main()