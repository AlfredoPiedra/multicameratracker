from sys import exit
from constants import DIRECTORY_PATH
from image_reader import ImageReader
from video_reader import VideoReader

'''
Detection Reader Class

Loads the detection bounding boxes coordinates from 
the corresponding files and the corresponding frame
from the abstraction layer

'''

class DetectionReader:

    # Inputs: 
    #           multimedia_source_type: string = valid types are video or image        
    #           multimedia_source_path: string = absolute path of the multimedia file
    #           detection_info_source_path: string =  absolute path of the detection file
    #
    def __init__(self,
                 multimedia_source_type,
                 multimedia_source_path,
                 detection_info_source_path):

        if multimedia_source_type == "video":

            self.__reader = VideoReader(full_video_path=multimedia_source_path)

        elif multimedia_source_type == "image":

            self.__reader = ImageReader(full_image_list_path=multimedia_source_path)

        else:

            print("[DetectionReader][init]: invalid multimedia source type, options are: image or video")
            exit()
            
        try:

            # The absolute path for the file that contains a list with all
            # the detection filenames
            self.__detection_info_source_path = detection_info_source_path
            
            # Counter for the reading of the files in the list
            self.__detection_info_counter = 1
            
            # Get the total number of the files specified in the list
            file = open(detection_info_source_path,"r")
            self.__detection_total_files = len(file.readlines())
            file.close()

            if(self.__detection_total_files != self.__reader.get_source_length()):

                print("[DetectionReader][init]: the specified detections_info_source_path file has a number of filenames(lines) = " 
                      + str(self.__detection_total_files)
                      + " which is different from the total frames of the media source = "
                      + str(self.__reader.get_source_length()))
                exit()

        except IOError:

            print("[DetectionReader][init]: could not open or read the detection info file")
            exit()

    # Output:
    #           ok: bool = stat flag of the frame read
    #           frame: cv::Mat = matrix with the image pixel values
    def get_next_frame(self): 

        # Get the frame from the abstraction layer
        ok,frame,_,_ = self.__reader.get_next_frame()
        return ok,frame

    # Input: 
    #        index: int = frame index value
    #
    # Output:
    #        ok: bool = stat flag of the frame read
    #        frame: cv::Mat = matrix with the image pixel values
    def get_frame_at(self,
                     index):

        # Get a specific frame from the abstraction layer
        ok,frame,_,_ = self.__reader.get_frame_at(index)
        return ok,frame


    # Input: 
    #        index: int = frame index value
    #
    # Output:
    #        bboxes_info: tuple array = the list with the coordinates of 
    #                                   all the detections for a specific frame
    def __read_detection_file_at(self,
                                 index):
        try:

            file = open(self.__detection_info_source_path,"r")
            detections_file_list = file.readlines()
            file.close()

            current_detection_file = detections_file_list[index]
            current_detection_file = (DIRECTORY_PATH
                                      + "/multimedia/"
                                      + current_detection_file.replace("\n","")
                                      + ".txt")

            file = open(current_detection_file,"r")
            bboxes_info = file.readlines()
            file.close()
            
            return bboxes_info

        except IOError:

            print("[DetectionReader][get_bboxes_at]: could not open or read the detection info file")
            exit()

    def __generate_cv2_format_bboxes(self,
                                     bboxes,
                                     image_width,
                                     image_height):

        cv2_format_bboxes = []

        # Convert the YOLO bbox format to opencv bbox format
        for bbox in bboxes:

            bbox_values = bbox.split()
                
            # Corresponding bounding box width
            bbox_width = float(bbox_values[3]) * image_width

            # Corresponding bounding box height
            bbox_height = float(bbox_values[4]) * image_height
                
            # x1 = xc - bbox_w/2
            left_top_corner_x = (float(bbox_values[1]) * image_width) - (bbox_width/2)

            # y1 = yc - h/2
            left_top_corner_y = (float(bbox_values[2]) * image_height) - (bbox_height/2)

            # Save the tuple as (x1,y1,bbox_width,bbox_height)
            cv2_format_bboxes += [(left_top_corner_x,left_top_corner_y,bbox_width,bbox_height)]

        return cv2_format_bboxes

    def get_next_bboxes(self):

        if self.__detection_info_counter <= self.__detection_total_files:

            reader_counter = self.__reader.get_reader_counter()

            # Frame total width and height
            _,_,img_height,img_width = self.__reader.get_frame_at(index=reader_counter)

            self.__reader.set_reader_counter(counter_value=reader_counter)

            if ((img_width <= 0) or
                (img_height <= 0)):

                print("[DetectionReader][get_next_bboxes]: could not get frame height and width")
                return False,None

            bboxes_info = self.__read_detection_file_at(index=self.__detection_info_counter-1)

            self.__detection_info_counter += 1

            cv2_format_bboxes = self.__generate_cv2_format_bboxes(
                                                                  bboxes=bboxes_info,
                                                                  image_width=img_width,
                                                                  image_height=img_height
                                                                 )
            return True,cv2_format_bboxes

        return False,None

    def get_bboxes_at(self,
                      index):

        # Frame total width and height
        ok,_,img_height,img_width = self.__reader.get_frame_at(index=index)

        if ((img_width <= 0)  or
            (img_height <= 0) or
            (not ok)):

            print("[DetectionReader][get_next_bboxes]: could not get frame height and width")
            return False,None

        bboxes_info = self.__read_detection_file_at(index=index-1)

        self.__detection_info_counter = index+1

        cv2_format_bboxes = self.__generate_cv2_format_bboxes(
                                                               bboxes=bboxes_info,
                                                               image_width=img_width,
                                                               image_height=img_height
                                                             )
        return True,cv2_format_bboxes

    def get_total_detection_files(self):

        return self.__detection_total_files

    def get_detection_reader_counter(self):

        return self.__detection_info_counter

    def set_detection_reader_counter(self,
                                     counter_value):

        # The media reader counter and the detection file 
        # info counter must be equal
        
        # First change the media reader counter, this function 
        # will check if the index value specified is valid 
        self.__reader.set_reader_counter(counter_value=counter_value)
        
        # If the previous line execute without a problem we know
        # that the index is valid, so just assign the value to
        # the detection info counter 
        self.__detection_info_counter = counter_value

    def get_media_reader_counter(self):

        return self.__reader.get_reader_counter()