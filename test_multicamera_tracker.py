from multicamera_tracker import MultiCameraTracker
from time import time

def main():

    tracker = MultiCameraTracker( 
                                  2,
                                  ["image","image"],
                                  ["camB_images_list.txt",
                                   "camC_images_list.txt"],
                                   ['B','C']
                                )

    #start = time()    
    #print("Running...")
    """for g in range(45):
        
        print("Frame: "+str(g))
        tracker.run_on_next_frame()"""    

    tracker.run_continuosly()
    #finish = time() - start
    #print("Total time: "+str(finish))

if __name__ == '__main__':

    main()
