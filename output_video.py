from input_format import InputFormat
from image_reader import ImageReader
from constants import DIRECTORY_PATH
from cv2 import VideoWriter, putText, rectangle, FONT_HERSHEY_SIMPLEX

class OutputVideo:

    def __init__(self, 
                 images_list_file,
                 results_list_file):
        
        self.__image_reader = ImageReader(full_image_list_path= DIRECTORY_PATH + '/multimedia/' + images_list_file)
        self.__input_reader = InputFormat()

        self.__results_file = results_list_file

    def create_video(self, output_video_name):

        track_results = self.__input_reader.read_results(filename=self.__results_file)

        _,image,height,width = self.__image_reader.get_next_frame()
        
        video = VideoWriter(output_video_name, 0 , 10, (width, height))

        for counter in range(len(track_results)):

            frame_index = track_results[counter]['frame_index']
            current_detections = track_results[counter]['detections']

            for bbox_key in current_detections:

                detection = current_detections[bbox_key]

                p1 = (int(detection[0]),int(detection[1]))
                p2 = (int(detection[0]+detection[2]),int(detection[1]+detection[3]))
            
                rectangle(image,p1,p2,(0,215,255),2,1)
                putText(image,"Frame: "+str(frame_index),(1100,40),FONT_HERSHEY_SIMPLEX,0.75,(150,170,50),2)
                putText(image, str(bbox_key),p1,FONT_HERSHEY_SIMPLEX,0.75,(0,0,255),2)

            counter += 1

            video.write(image)
            _,image,_,_ = self.__image_reader.get_next_frame()

        video.release()

if __name__ == '__main__':

    ov = OutputVideo(images_list_file= 'camC_images_list.txt',
                     results_list_file= DIRECTORY_PATH + '/trackingResults/C_20191112-074710.txt')
    
    ov.create_video(output_video_name= DIRECTORY_PATH + '/trackingResults/CFinal.avi')