from sys import exit
from mht import MHT
from time import strftime
from output_format import OutputFormat
from primary_trackers import PrimaryTrackers
from detection_reader import DetectionReader
from constants import DIRECTORY_PATH,MHT_PARAMS,SELECTED_TRACKERS

class SingleCameraTracker:

    def __init__(self,
                 media_source,
                 media_filename,
                 tracker_id,
                 save_frecuency=1,
                 output_format=None):

        self.__detection_reader = DetectionReader( 
                                                    multimedia_source_type=media_source,
                                                    multimedia_source_path=DIRECTORY_PATH + "/multimedia/" + media_filename,
                                                    detection_info_source_path=DIRECTORY_PATH + "/multimedia/" + media_filename
                                                 )

        # Multitracker object (primary trackers)
        self.__primary_trackers = PrimaryTrackers(selected_trackers_list=SELECTED_TRACKERS)

        if len(MHT_PARAMS['trackers_weights']) != len(SELECTED_TRACKERS):

            print("[SingleCameraTracker][init]: not every tracker algorithm has a corresponding weight value")
            exit()

        self.__mht = MHT(params=MHT_PARAMS)

        self.__mht_ids_to_trackers_ids_dict = {}

        self.__save_frequency = save_frecuency

        self.__tracking_results_buffer = []

        self.__output_writer = OutputFormat(
                                            output_filename=tracker_id + '_' + strftime("%Y%m%d-%H%M%S") + '.txt',
                                            format_option=output_format
                                           )
        self.__tracker_init()

    def __tracker_init(self):

        # Get the first frame
        ok,frame = self.__detection_reader.get_frame_at(1)
        
        if not ok:
                
            print("[SingleCameraTracker][tracker_init]: could not get the first frame")
            exit()

        # Get the first frame bboxes in the opencv required format
        ok,bboxes = self.__detection_reader.get_bboxes_at(1)
        
        if not ok:

            print("[SingleCameraTracker][tracker_init]: could not get the first bbox info")
            exit()
                
        # Initialization of primary trackers for each target
        for bbox in bboxes:

            self.__primary_trackers.add_tracking_target(
                                                         initial_frame=frame,
                                                         initial_bbox=bbox
                                                       )
                
        mht_tracks_initial = self.__mht.init(bboxes)

        self.__output_writer.save_result(data_dictionary_list=[mht_tracks_initial])

        mht_tracks_ids = sorted(list(mht_tracks_initial['detections'].keys()))

        for i in range(len(bboxes)):

            self.__mht_ids_to_trackers_ids_dict[mht_tracks_ids[i]] = i
   
    def __assign_detection_to_id(self,
                                 trackers_result):
        
        id_detections_dict = self.__mht_ids_to_trackers_ids_dict.copy()

        for key in id_detections_dict.keys():

            pos_in_trackers_result = id_detections_dict[key]
            id_detections_dict[key] = trackers_result[pos_in_trackers_result]    

        return id_detections_dict

    def __execute_tracking(self,
                           frame,
                           bboxes,
                           external_hypothesis_list):

        # Update the primary trackers for the current frame
        trackers_result = self.__primary_trackers.get_tracking_results(frame=frame)

        tracker_result_id_dict = self.__assign_detection_to_id(trackers_result=trackers_result)

        # Run MHT with annotations (detections) and tracker results
        solution_dict, new_tracks, global_hypothesis = self.__mht.run( 
                                                                      detections_list=bboxes,
                                                                      trackers_results_dict=tracker_result_id_dict,
                                                                      external_hypothesis_list=external_hypothesis_list
                                                                     )

        # Update primary trackers when they're lost or there are new targets
        if len(new_tracks) != 0:
                    
            for key in new_tracks.keys():

                self.__mht_ids_to_trackers_ids_dict[key] = max(self.__mht_ids_to_trackers_ids_dict.values()) + 1

                # Append new trackers for the target
                new_box = new_tracks[key]

                self.__primary_trackers.add_tracking_target(
                                                             initial_frame=frame,
                                                             initial_bbox=new_box
                                                           )
        return solution_dict, global_hypothesis
               
    def run_on_next_frame(self,
                          external_hypothesis_list=None):

        # Read the next bboxes info
        ok_bbox,bboxes = self.__detection_reader.get_next_bboxes()

        # Read the next frame
        ok_frame,frame = self.__detection_reader.get_next_frame()

        if not (ok_frame and ok_bbox):

            return False, None, None
    
        # Execute the tracking process for the current frame
        # and get the corresponding results
        solution_dict, global_hypothesis = self.__execute_tracking(
                                                                    frame=frame,
                                                                    bboxes=bboxes,
                                                                    external_hypothesis_list=external_hypothesis_list
                                                                  )
        return True, solution_dict, global_hypothesis

    def save_frame_results(self,
                           solution_dict, 
                           force_save=False):


        if not force_save:

            # Save the tracking results of the current frame in the buffer
            self.__tracking_results_buffer.append(solution_dict)

            # Save the results only if the counter 
            # is equal to the specified frequency
            if len(self.__tracking_results_buffer) == self.__save_frequency:

                # Save the tracking results that are in the buffer
                self.__output_writer.save_result(data_dictionary_list=self.__tracking_results_buffer)

                # Reset the values of the frame counter and the results buffer
                self.__tracking_results_buffer = []

        else:

            # Save the tracking results that are in the buffer
            self.__output_writer.save_result(data_dictionary_list=self.__tracking_results_buffer)

            # Reset the values of the frame counter and the results buffer
            self.__tracking_results_buffer = []

    def get_reader_counter(self):

        return self.__detection_reader.get_detection_reader_counter()

    def get_detections_at(self,
                          frame_index):
    
        # Get the current counter value, to change back the modified counter of the reader
        current_counter_value = self.__detection_reader.get_detection_reader_counter()

        # Read the bboxes associated to the frame index specified
        ok,bboxes = self.__detection_reader.get_bboxes_at(index=frame_index)

        # Set the value of the reader counter to its previous value
        self.__detection_reader.set_detection_reader_counter(counter_value=current_counter_value)

        # In case the frame index was invalid, stop the execution
        if not ok:
            
            print("[SingleCameraTracker][get_detections_at]: invalid index value: " + str(frame_index))
            return []

        # Return the number of objects detected in the frame specified
        return bboxes