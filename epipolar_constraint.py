from sys import exit
import numpy as np
from numpy.linalg import inv
from camera_model import CameraModel

class EpipolarConstraint:

    def __init__(self,
                 left_camera_id,
                 right_camera_id):

        self.__right_camera = CameraModel(right_camera_id)
        self.__left_camera = CameraModel(left_camera_id)
        self.__F = self.__get_fundamental_matrix()
        self.__cameras_ids = [(left_camera_id,right_camera_id),(right_camera_id,left_camera_id)]

    def __get_cross_product_matrix(self,
                                   vector_notation):

        vx = vector_notation[0]
        vy = vector_notation[1]
        vz = vector_notation[2]

        matrix_notation = np.array([
                                    [0,-vz,vy],
                                    [vz,0,-vx],
                                    [-vy,vx,0]
                                  ])
        return matrix_notation

    def __get_fundamental_matrix(self):

        RL = self.__left_camera.get_R()
        RR = self.__right_camera.get_R()
        tL = self.__left_camera.get_t()
        tR = self.__right_camera.get_t()
        KL = self.__left_camera.get_K()
        KR = self.__right_camera.get_K()

        R_total = np.matmul(RR,inv(RL))
        t_total = np.matmul(-RR,np.matmul(inv(RL),tL)) + tR

        Mt_total = self.__get_cross_product_matrix(t_total)

        E = np.matmul(Mt_total,R_total)

        F = np.matmul(inv(KR).transpose(),E)
        F = np.matmul(F,inv(KL))

        return F

    def get_point_line_distance(self,
                                point,
                                line):
        a = line[0]
        b = line[1]
        c = line[2]

        x1 = point[0]
        y1 = point[1]

        distance = np.abs(a * x1 + b * y1 + c) / np.sqrt( a * a + b * b)

        return distance

    def get_right_camera_epilines(self,
                                  left_camera_points):

        epipolar_lines = []

        for point in left_camera_points:

            normal_point = point + (1,)

            epipolar_line = np.matmul(self.__F,normal_point)
            epipolar_lines.append(epipolar_line)

        return epipolar_lines

    def get_left_camera_epilines(self,
                                 right_camera_points):

        epipolar_lines = []

        for point in right_camera_points:

            normal_point = point + (1,)

            epipolar_line = np.matmul(normal_point,self.__F)
            epipolar_lines.append(epipolar_line)

        return epipolar_lines

    def get_cameras_ids(self):

        return self.__cameras_ids 