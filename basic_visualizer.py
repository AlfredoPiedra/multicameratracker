from cv2 import imread,rectangle,putText,imshow,waitKey,destroyAllWindows,FONT_HERSHEY_SIMPLEX,circle
from input_format import InputFormat
from constants import DIRECTORY_PATH,TRACKERS_OUTPUT_DIRECTORY

def show_track_results(image_list_filename,
                       track_results_filename):


    absolute_image_filename = DIRECTORY_PATH + '/multimedia/' + image_list_filename

    images_file = open(absolute_image_filename,'r')
    image_list = images_file.readlines()
    images_file.close()

    track_results_reader = InputFormat()

    absolute_trackers_filename = DIRECTORY_PATH + '/' + TRACKERS_OUTPUT_DIRECTORY + '/' + track_results_filename

    track_results = track_results_reader.read_results(filename=absolute_trackers_filename)

    if(len(track_results) > len(image_list)):

        print('[show_track_results]: different number of images and detections')
        return

    centers = []
    colors = [(255,0,255),(0,255,255),(0,255,0),(0,0,255)]
    ci = 0
    for i in range(len(track_results)):

        frame_filename = DIRECTORY_PATH + '/multimedia/' + image_list[i].replace("\n","") + ".png"

        frame = imread(filename=frame_filename)

        frame_index = track_results[i]['frame_index']

        current_detections = track_results[i]['detections']

        for bbox_key in current_detections:

            detection = current_detections[bbox_key]

            p1 = (int(detection[0]),int(detection[1]))
            p2 = (int(detection[0]+detection[2]),int(detection[1]+detection[3]))

            xc = (p1[0]+p2[0])/2
            yc = (p1[1]+p2[1])/2
            centers.append((int(xc), int(yc)))

            rectangle(frame,p1,p2,(0,215,255),2,1)
            putText(frame,"Frame: "+str(frame_index),(1100,40),FONT_HERSHEY_SIMPLEX,0.75,(150,170,50),2)
            putText(frame,str(bbox_key),p1,FONT_HERSHEY_SIMPLEX,0.75,(0,0,255),2)

        for point in centers:

            circle(frame,point,1,colors[ci],1)
            ci += 1
            if ci == len(colors):
                ci = 0

        imshow("Single Camera Tracker",frame)
        waitKey(100)

if __name__ == '__main__':

    for _ in range(100):

        show_track_results(
                            image_list_filename='camC_images_list.txt',
                            #track_results_filename='A_20191016-131431.txt',
                            track_results_filename='C_20191112-081842.txt'
                            #track_results_filename='C_20191112-074710.txt'
                          )

    destroyAllWindows()    
