from os import path
from cv2 import (TrackerBoosting_create,
                 TrackerCSRT_create,
                 TrackerGOTURN_create,
                 TrackerKCF_create,
                 TrackerMIL_create,
                 TrackerMedianFlow_create,
                 TrackerTLD_create,
                 TrackerMOSSE_create)

CAMERA_IDS = [

  "A","B","C","D","F","G",
  "H","I","J","K","L","M",
  "N","O","P","Q","R","S",
  "T","U","V","W","X","Y","Z"
  
]

CAMERA_BLENDER_MAPPING_DICT = {
  
  "1":"C","2":"B","3":"A",
  "4":"C","5":"B","6":"A",
  "7":"C","8":"B","9":"A"

} 

DIRECTORY_PATH = path.dirname(path.realpath(__file__))

PROBABILITY_MATRIX_DICT_MAX_SIZE = 2

# MHT
MHT_PARAMS = {

  "N_pruning": 0,            # Index for pruning
  "distance_threshold": 100.0, # Distance threshold for hypothesis formation
  "distance_threshold2": 75.0, # Distance threshold for the updating of primary trackers
  "trackers_weights": [0.45,0.55]
  #"trackers_weights": [0.2,0.35,0.45]      

}

CV2_TRACKERS_DICT = {

  "MIL":        TrackerMIL_create,
  "KCF":        TrackerKCF_create,
  "TLD":        TrackerTLD_create,
  "CSRT":       TrackerCSRT_create,
  "MOSSE":      TrackerMOSSE_create,
  "GOTURN":     TrackerGOTURN_create, 
  "BOOSTING":   TrackerBoosting_create,
  "MEDIANFLOW": TrackerMedianFlow_create

}

SELECTED_TRACKERS = [

  "KCF",
  "MIL"
  
]

OUTPUT_FORMATS = {
  
  "yolo":"YOLO"
  
}

TRACKERS_OUTPUT_DIRECTORY = "trackingResults"