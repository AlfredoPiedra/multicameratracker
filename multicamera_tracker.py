from numpy import exp
from sys import exit
from munkres import Munkres
from itertools import product,combinations,chain
from epipolar_constraint import EpipolarConstraint
from singlecamera_tracker import SingleCameraTracker

import cv2

class MultiCameraTracker:

    def __init__(self,
                 number_of_cameras,
                 media_source_type_list,
                 media_filename_list,
                 cameras_ids_list,
                 sigma=2.0):

        if(number_of_cameras < 2):

            print("[MultiCameraTracker][init]: number_of_camera must be greater than or equal to 2")
            exit()

        if ((len(media_filename_list) != number_of_cameras) or 
              (len(media_source_type_list) != number_of_cameras) or
              (len(cameras_ids_list) != number_of_cameras)):

            print("[MultiCameraTracker][init]: media_filename_list and media_source_type_list" 
                  +" must have a number of elements equal to number_of_cameras: "+str(number_of_cameras))
            exit()

        self.__cameras = []
        self.__cameras_ids_list = cameras_ids_list
        self.__probability_matrix_list = []
        self.__sigma = sigma
        self.__epipolar_constraints = []
        self.__previous_frame_det = []
        self.__global_detections_ids_dict = {}

        for i in range(number_of_cameras):

            self.__cameras.append(
                                  SingleCameraTracker(
                                                       media_source = media_source_type_list[i],
                                                       media_filename = media_filename_list[i],
                                                       tracker_id=cameras_ids_list[i],
                                                       save_frecuency=10
                                                     )
                                 )

        for i in range(number_of_cameras):    

            for j in range(i+1,number_of_cameras):

                self.__epipolar_constraints.append(
                                                   EpipolarConstraint(
                                                                      left_camera_id=cameras_ids_list[i],
                                                                      right_camera_id=cameras_ids_list[j]
                                                                     )
                                                  )
        self.__init_probability_matrix()

    def __init_probability_matrix(self):

        for camera_tracker in self.__cameras:
            
            # Get the first frame detections from the corresponding
            # file and store it 
            first_frame_detections = camera_tracker.get_detections_at(1)

            # In case something goes wrong while reading the files.
            if len(first_frame_detections) == 0:

                print("[MultiCameraTracker][init_probability_matrix]: Error while reading detections file")
                exit()

            # Save the number of the detections in the camera
            # for the current frame
            # This information is stored in order to avoid read 
            # again the previous frame detection file to know 
            # the number of detections that contains in the function
            #  generate_epipolar_hypothesis.
            self.__previous_frame_det.append(first_frame_detections)
        
        first_combinations = list(combinations(self.__previous_frame_det,2))
        first_keys = list(combinations(self.__cameras_ids_list,2))
        first_dictionary = {}

        for constraint, key, combination in zip(self.__epipolar_constraints,
                                                first_keys,
                                                first_combinations):

            first_dictionary[key] = self.__generate_probability_matrix(

                left_camera_bboxes= combination[0],
                right_camera_bboxes= combination[1],
                epipolar_constraint= constraint
            )
        
        self.__probability_matrix_list = [0,first_dictionary]

    def __get_bbox_center(self,
                          bbox):

        # Get bboxes centers
        x1 = bbox[0]
        y1 = bbox[1]
        x2 = bbox[2]+ bbox[0]
        y2 = bbox[3]+ bbox[1]
        
        xc = (x1+x2)/2
        yc = (y1+y2)/2
        center = (xc, yc)
                                
        return center

    def __get_epipolar_constraint_score(self,
                                        epipolar_line,
                                        detection_points):

        scores = []
        
        for point in detection_points:

            distance = self.__epipolar_constraints[0].get_point_line_distance(point=point,
                                                                              line=epipolar_line)
            score = exp(-0.5 * (distance/self.__sigma) * (distance/self.__sigma))

            scores.append(score)

        return scores

    def __generate_probability_matrix(self,
                                      left_camera_bboxes,
                                      right_camera_bboxes,
                                      epipolar_constraint):

        probability_matrix = []
        
        left_camera_points = []
        right_camera_points = []

        for l_bbox in left_camera_bboxes:

            left_camera_points.append(self.__get_bbox_center(bbox=l_bbox))

        for r_bbox in right_camera_bboxes:

            right_camera_points.append(self.__get_bbox_center(bbox=r_bbox))


        right_camera_lines = epipolar_constraint.get_right_camera_epilines(
                                                                            left_camera_points=left_camera_points
                                                                          )
  
        left_camera_lines = epipolar_constraint.get_left_camera_epilines(
                                                                          right_camera_points=right_camera_points
                                                                        )

        for r_line in right_camera_lines:

            row = self.__get_epipolar_constraint_score(
                                                       epipolar_line=r_line,
                                                       detection_points=right_camera_points
                                                      )     
            probability_matrix.append(row)

        for i,l_line in enumerate(left_camera_lines):

            column = self.__get_epipolar_constraint_score(
                                                           epipolar_line=l_line,
                                                           detection_points=left_camera_points
                                                         )

            for j in range(len(column)):

                probability_matrix[j][i] = min(probability_matrix[j][i],column[j])
                
                #probability_matrix[j][i] = ( probability_matrix[j][i] + column[j] ) / 2
                #probability_matrix[j][i] *= column[j]                 

        return probability_matrix

    def __get_all_combinations(self,
                               previous_detections,
                               current_detections):

        current_bboxes_combinations = list(combinations(current_detections,2))
        cross_bboxes_combinations = []

        for i in range(len(current_detections)):
 
            sublist = current_detections[:i] + current_detections[i+1:]
            sublist = list(product([previous_detections[i]],sublist))
            cross_bboxes_combinations += sublist

        return current_bboxes_combinations, cross_bboxes_combinations
    
    def __get_corresponding_constraint(self,
                                       cameras_ids):

        reverse = False

        for constraint in self.__epipolar_constraints:

            if cameras_ids in constraint.get_cameras_ids():

                if cameras_ids == constraint.get_cameras_ids()[1]:

                    reverse = True

                return constraint, reverse

        return None, reverse

    def __update_probability_matrix_list(self,
                                         previous_detections,
                                         current_detections):
        (curr_combinations, 
         cross_combinations) = self.__get_all_combinations(previous_detections=previous_detections,
                                                           current_detections=current_detections)
        (curr_keys,
         cross_keys) = self.__get_all_combinations(previous_detections= self.__cameras_ids_list,
                                                   current_detections= self.__cameras_ids_list)

        curr_dictionary = {}
        cross_dictionary = {}

        for constraint, key, combination in zip(self.__epipolar_constraints,
                                                curr_keys,
                                                curr_combinations):

            curr_dictionary[key] = self.__generate_probability_matrix(

                left_camera_bboxes= combination[0],
                right_camera_bboxes= combination[1],
                epipolar_constraint= constraint
            )

        for key, combination in zip(cross_keys,
                                    cross_combinations):

            constraint, reverse = self.__get_corresponding_constraint(cameras_ids=key)

            if constraint != None:

                if reverse:

                    left = combination[1]
                    right = combination[0] 
                
                else:

                    left = combination[0]
                    right = combination[1]

                cross_dictionary[key] = self.__generate_probability_matrix(

                    left_camera_bboxes=left,
                    right_camera_bboxes=right,
                    epipolar_constraint= constraint
                )
            
            else:

                print('[MultiCameraTracker][update_probability_matrix_list]: constraint is null for: ' + str(key))
                exit()

        self.__probability_matrix_list = [self.__probability_matrix_list[1],
                                          curr_dictionary,
                                          cross_dictionary]

    def __compute_hypothesis_score(self,
                                   combination):

        previous_detection_ids = []
        current_detection_ids = []
        score = 1.0

        for camera_hypothesis in combination:

            previous_detection_ids.append(camera_hypothesis[0])
            current_detection_ids.append(camera_hypothesis[1])

        prev_ids = list(combinations(previous_detection_ids,2))

        ( curr_ids,
          cross_ids ) = self.__get_all_combinations(previous_detections=previous_detection_ids,
                                                     current_detections=current_detection_ids)

        ( curr_keys,
          cross_keys ) = self.__get_all_combinations(previous_detections=self.__cameras_ids_list[:len(combination)],
                                                     current_detections=self.__cameras_ids_list[:len(combination)])


        for key, prev, curr in zip(curr_keys,
                                   prev_ids,
                                   curr_ids):

            if (prev[0] != -1 and prev[1] != -1):
                
                score *= self.__probability_matrix_list[0][key][prev[0]][prev[1]]
                          

            if (curr[0] != -1 and curr[1] != -1):

                score *= self.__probability_matrix_list[1][key][curr[0]][curr[1]]
                
                            

        for key, cross in zip(cross_keys,
                              cross_ids):

            if cross[0] != -1 and cross[1] != -1:

                _, reverse = self.__get_corresponding_constraint(cameras_ids=key)

                if reverse:

                    left = cross[1]
                    right = cross[0]

                else:

                    left = cross[0]
                    right = cross[1]

                score *= (self.__probability_matrix_list[2][key][left][right])

        return score

    def __generate_epipolar_hypothesis(self):

        # List used to store the bboxes 
        # of the current frame
        current_bboxes = []

        # This will be a matrix used to store
        # all the combinations associated to
        # a camera between the previous and
        # the current frame.
        # Each row correspond to a camera, 
        # in the order specified by the user
        cameras_combinations = []

        # List used to save the accepted  
        # hypothesis and return it
        camera_accepted_hypothesis = []

        cameras_correspondence_score_list = []
        
        for cam_index,camera_tracker in enumerate(self.__cameras):

            # Initialize the structure needed to store the results
            camera_accepted_hypothesis.append([[],[]])

            # Get the index of the actual frame we are analyzing.
            # This value is needed in order to know what files we need
            # to read in order to get the actual frame detections
            current_counter_value = camera_tracker.get_reader_counter()
            
            # Get the current frame detections from the corresponding
            # file and store it 
            actual_detections = camera_tracker.get_detections_at(current_counter_value)

            # In case something goes wrong while reading the files.
            if len(actual_detections) == 0:

                return [[]]

            # We need to calculate all the possible combinations between
            # the detections, so first we need to store it all in a list.
            # As the function that reads the detections generates a list
            # with tuples, we are storing lists inside a list, so 
            # in other words we are creating a matrix

            # Store the current detections list 
            # as a row in the current detections matrix.
            current_bboxes.append(actual_detections)

            # Set the detection id, every detection is identified by the
            # position in the list, which is equal to the number of the
            # line of the file where was it read
            previous_ids = list(range(len(self.__previous_frame_det[cam_index])))
            actual_ids = list(range(len(actual_detections)))

            # Compute all the possible combinations (using the detections ids)
            # between the detections in the previous and the current frame.
            # This will be the base for the epipolar hypothesis creation 
            all_combinations = list(product(previous_ids,actual_ids))

            # Save the possible combinations for the objects inside 
            # a camera as a row in the combination matrix
            cameras_combinations.append(all_combinations)

            cameras_correspondence_score_list.append([])

            for _ in range(len(self.__previous_frame_det[cam_index])):

                cameras_correspondence_score_list[cam_index].append([1.0] * len(actual_detections))
    
        # Add the current probability matrix to the list
        # because it will be needed for the epipolar score
        # calculation
        self.__update_probability_matrix_list(previous_detections= self.__previous_frame_det,
                                              current_detections= current_bboxes)

        # By the time this function is executed again
        # the current detections will became the previous
        # detections, so is mandatory to update the 
        # previous frame total detections in the current execution
        self.__previous_frame_det = current_bboxes

        # Create the munkres class required instance
        munkres_object = Munkres()

        nested_list = False

        for _ in range(len(self.__cameras) - 1):

            # Initialization of the munkres matrix used
            # to store the scores and compute the minimum 
            # weighted combination
            munkres_matrix = []
            combinations_matrix = []
            hypothesis_row = cameras_combinations[0]
            hypothesis_column  = cameras_combinations[1]

            for row in hypothesis_row:

                munkres_row = []
                combinations_row = []
                
                for column in hypothesis_column:

                    if nested_list:

                        combination = list(chain(*[row,[column]]))
                        score = self.__compute_hypothesis_score(combination=combination)

                    else:
                        combination = [row,column]
                        score = self.__compute_hypothesis_score(combination=combination)

                    munkres_row.append(1.0 - score)
                    combinations_row.append(combination)
                
                munkres_matrix.append(munkres_row)
                combinations_matrix.append(combinations_row)

            nested_list = True
            selected_hypothesis_indexes = munkres_object.compute(munkres_matrix)

            merge_hypothesis_list = []
            current_hypothesis_scores = []

            for index in selected_hypothesis_indexes:

                merge_hypothesis_list.append(combinations_matrix[index[0]][index[1]])
                current_hypothesis_scores.append(munkres_matrix[index[0]][index[1]])

            cameras_combinations = [merge_hypothesis_list] + cameras_combinations[2:]

        for h_index, hypothesis in enumerate(cameras_combinations[0]):

            for p_index,point in enumerate(hypothesis):

                cameras_correspondence_score_list[p_index][point[0]][point[1]] = current_hypothesis_scores[h_index] 

        for m_index,munkres_score_matrix in enumerate(cameras_correspondence_score_list):

            selected_indexes = munkres_object.compute(munkres_score_matrix)

            for s_index in selected_indexes:

                sel_score = 1.0 - munkres_score_matrix[s_index[0]][s_index[1]]
                
                #if sel_score > 0.0:

                camera_accepted_hypothesis[m_index][0].append(s_index)
                camera_accepted_hypothesis[m_index][1].append(sel_score)

        return camera_accepted_hypothesis
        
    def run_on_next_frame(self):

        accepted_hypothesis_list = self.__generate_epipolar_hypothesis()

        ok  = True
        
        hypothesis_list = [None] * len(self.__cameras)
        cams_selected_hypothesis = []
        cams_solution_dict_list = []
        # Create the munkres class required instance
        munkres_object = Munkres()
        nested_list = False

        if len(accepted_hypothesis_list) > 0:

            if len(accepted_hypothesis_list[0]) > 0:

                hypothesis_list = accepted_hypothesis_list

        for index,hypothesis in enumerate(hypothesis_list):

            cam_ok, solution_dict, cam_global_hypothesis = self.__cameras[index].run_on_next_frame(external_hypothesis_list=hypothesis)
            cams_selected_hypothesis.append(cam_global_hypothesis)
            cams_solution_dict_list.append(solution_dict)
            if not cam_ok:
            
                self.__cameras[index].save_frame_results(solution_dict={},
                                                         force_save=True)
            
            ok = ok and cam_ok

        if ok:

            for _ in range(len(self.__cameras) - 1):

                # Initialization of the munkres matrix used
                # to store the scores and compute the minimum 
                # weighted combination
                munkres_matrix = []
                combinations_matrix = []
                hypothesis_row = cams_selected_hypothesis[0]
                hypothesis_column  = cams_selected_hypothesis[1]

                for row in hypothesis_row:

                    munkres_row = []
                    combinations_row = []
                    
                    for column in hypothesis_column:

                        if nested_list:

                            combination = list(chain(*[row,[column]]))
                            score = self.__compute_hypothesis_score(combination=combination)

                        else:
                            combination = [row,column]
                            score = self.__compute_hypothesis_score(combination=combination)

                        munkres_row.append(1.0 - score)
                        combinations_row.append(combination)
                    
                    munkres_matrix.append(munkres_row)
                    combinations_matrix.append(combinations_row)

                selected_hypothesis_indexes = munkres_object.compute(munkres_matrix)

                merge_hypothesis_list = []
                current_hypothesis_scores = []
                row_ids = []
                col_ids = []

                for index in selected_hypothesis_indexes:

                    merge_hypothesis_list.append(combinations_matrix[index[0]][index[1]])
                    current_hypothesis_scores.append(munkres_matrix[index[0]][index[1]])

                    row_ids.append(index[0])
                    col_ids.append(index[1])

                if len(selected_hypothesis_indexes) < max(len(hypothesis_row),len(hypothesis_column)):

                    total_row_ids = list(range(len(hypothesis_row)))
                    total_col_ids = list(range(len(hypothesis_column)))

                    for r_id in total_row_ids:

                        if r_id not in row_ids:

                            if nested_list:

                                hypothesis_row[r_id].append((-1,-1))
                                merge_hypothesis_list.append(hypothesis_row[r_id])

                            else:

                                merge_hypothesis_list.append([hypothesis_row[r_id],(-1,-1)])

                    for c_id in total_col_ids:

                        if c_id not in col_ids:

                            new_hypothesis = [(-1,-1)] * (len(merge_hypothesis_list[0]) - 1)
                            new_hypothesis.append(hypothesis_column[c_id])
                            merge_hypothesis_list.append(new_hypothesis)

                cams_selected_hypothesis = [merge_hypothesis_list] + cams_selected_hypothesis[2:]
                
                nested_list = True

            if len(self.__global_detections_ids_dict) > 0:

                for hypothesis in cams_selected_hypothesis[0]:

                    any_match = False

                    for key in self.__global_detections_ids_dict.keys():

                        if len(hypothesis) == len(self.__global_detections_ids_dict[key]):

                            match = False

                            for h_point, p_point in zip(hypothesis,
                                                        self.__global_detections_ids_dict[key]):

                                match = match or (h_point[0] == p_point[1])
                                                    
                            if match:

                                if hypothesis not in list(self.__global_detections_ids_dict.values()):
                                    
                                    self.__global_detections_ids_dict[key] = hypothesis

                            any_match = any_match or match

                    if not any_match:

                        new_id = max(self.__global_detections_ids_dict.keys()) + 1
                        self.__global_detections_ids_dict[new_id] = hypothesis

            else:

                for h_id in range(len(cams_selected_hypothesis[0])):

                    self.__global_detections_ids_dict[h_id] = cams_selected_hypothesis[0][h_id]

            for dict_index in range(len(cams_solution_dict_list)):

                new_solution_dict = {}
                local_solution_dict = cams_solution_dict_list[dict_index]['detections']

                for key in local_solution_dict.keys():

                    for g_key in self.__global_detections_ids_dict.keys():

                        if self.__global_detections_ids_dict[g_key][dict_index][1] == key:

                            new_solution_dict[g_key] = local_solution_dict[key]
                            break

                cams_solution_dict_list[dict_index]['detections'] = new_solution_dict

            for camera,solution_dict in zip(self.__cameras,
                                            cams_solution_dict_list):

                camera.save_frame_results(solution_dict=solution_dict)

        return ok

    def run_continuosly(self):

        keep_running = True

        frame_count = 0

        while keep_running:

            print('Frame: ' + str(frame_count))
            frame_count += 1
            keep_running = self.run_on_next_frame()
