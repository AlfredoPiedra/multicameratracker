import logging
import numpy as np

from copy import deepcopy

# Class for each hypothesis
from track_wrapper import Track                            

# MWIS algorithm code
from weighted_graph import WeightedGraph                

# Messages on terminal
logging.basicConfig(level = logging.INFO,               
                    format = '%(asctime)s %(message)s',
                    datefmt = '%H:%M:%S')

class MHT:

    def __init__( self,
                  params ):

        # Load parameters
        
        self.__N = params['N_pruning']

        self.__d_th = params['distance_threshold']

        self.__d_th2 = params['distance_threshold2']

        self.__trackers_weights = params['trackers_weights']
        
        # Track detections over all the frames
        self.__track_detections = []

        # Corresponding objects Track
        self.__tracks = []

        self.__frame_index = 0

        # Used to set an ID to each object Track
        self.__traject_count = 0                         

    def init( self,
              detections ):

        tracks_dict = {}

        # Initialization of tracks in the first frame
        for index in range(0,len(detections)):

            detection_id = str(index)

            self.__tracks.append( Track( init_track_id=self.__traject_count,
                                       init_detection=detections[index] ))

            tracks_dict[self.__traject_count] = detections[index]

            self.__traject_count += 1
                               
            self.__track_detections.append([''] * self.__frame_index + [detection_id])

        initial_output = self.__get_output_format(data_dictionary=tracks_dict)

        self.__frame_index += 1

        return initial_output

    def __get_output_format(self,
                            data_dictionary):

        output_dictionary = {
                             'frame_index' : self.__frame_index,
                             'detections'  : data_dictionary
                            }

        return output_dictionary

    def __update_tracks(self,
                        detections,
                        trackers_results,
                        external_hypothesis_list):

        track_count = len(self.__tracks)

        for index in range(len(detections)):

            detection_id = str(index)
                                
            # Update existing branches
            for i in range(track_count):

                # Copy the track hypothesis
                track_tree = self.__tracks[i]
                continued_branch = deepcopy(track_tree)

                track_id = continued_branch.get_track_id()
            
                # Get track score based on distances
                inside, score, trackers_lost = self.__get_trackers_score( detection=detections[index],
                                                                          tracker_results=trackers_results[track_id])

                # Create new hypothesis only if, at least, one of the primary trackers are inside the gating area
                if inside:

                    continued_branch.update( detection=detections[index],
                                             score=score,
                                             trackers_lost=trackers_lost)
                                
                    self.__tracks.append(continued_branch)
                    self.__track_detections.append(self.__track_detections[i] + [detection_id])
            
        if external_hypothesis_list != None:

            if (len(external_hypothesis_list[0]) > 0 and
                len(external_hypothesis_list[1]) > 0):

                factor = 10

                trackers_hypothesis = self.__track_detections[track_count:]
                base_hypothesis = self.__track_detections[:track_count]
                external_hypothesis = external_hypothesis_list[0]
                external_scores = external_hypothesis_list[1]

                for h in range(len(trackers_hypothesis)):

                    if trackers_hypothesis[h][-2] != '':

                        last_value = int(trackers_hypothesis[h][-2])

                    else:

                        last_value = -1

                    trackers_hypothesis[h] = (last_value,
                                              int(trackers_hypothesis[h][-1]))

                for h in range(len(base_hypothesis)):

                    if base_hypothesis[h][-1] != '':
                        
                        last_value = int(base_hypothesis[h][-1])

                    else:

                        last_value = -1

                    base_hypothesis[h] = last_value
                
                for i,hypothesis in enumerate(external_hypothesis):

                    if hypothesis in trackers_hypothesis:

                        position = trackers_hypothesis.index(hypothesis)

                        actual_score = self.__tracks[track_count+position].get_track_score()

                        combined_score = actual_score + factor * external_scores[i]

                        self.__tracks[track_count+position].set_track_score(new_score=combined_score)

                    else:

                        if hypothesis[0] in base_hypothesis:

                            position = base_hypothesis.index(hypothesis[0])

                            continued_branch = deepcopy(self.__tracks[position])

                            continued_branch.set_track_score(new_score=continued_branch.get_track_score() 
                                                            + factor * external_scores[i])

                            continued_branch.set_last_detection(new_detection=detections[hypothesis[1]])

                            self.__tracks.append(continued_branch)
                            self.__track_detections.append(self.__track_detections[position] + [str(hypothesis[1])])
        
        for index in range(len(detections)):

            # Create new branch from the detection (new target possibility)
            self.__tracks.append( Track( init_track_id=self.__traject_count,
                                         init_detection=detections[index]))
            self.__traject_count += 1
            self.__track_detections.append([''] * self.__frame_index + [str(index)])

        # Update the track with a dummy detection (lost target possibility)
        for j in range(track_count):

            self.__tracks[j].update(  detection=None,
                                      score=None,
                                      trackers_lost=None)
                                
            self.__track_detections[j].append('')
        
    def __prune_track_tree(self,
                           solution_ids,
                           detections):

        non_solution_ids = list(set(range(len(self.__tracks))) - set(solution_ids))

        # Index for N-scan pruning
        prune_index = max(0,
                          self.__frame_index-self.__N)
        track_coordinates = {}
        prune_ids = set() 

        for solution_id in solution_ids:
            
            s_detections = self.__track_detections[solution_id]
            
            if s_detections[-1] != '':

                track_coordinates[int(s_detections[-1])] = detections[int(s_detections[-1])]   
            
            # Identify subtrees that diverge from the solution_trees at frame k-N
            if self.__N > 0:

                d_id = self.__track_detections[solution_id][prune_index]

                if d_id != '':

                    for non_solution_id in non_solution_ids:

                        if d_id == self.__track_detections[non_solution_id][prune_index]:

                            prune_ids.add(non_solution_id)
          
        # Perform pruning
        if self.__N == 0:
            
            prune_ids = non_solution_ids

        for k in sorted(prune_ids, reverse=True):

            del self.__track_detections[k]
            del self.__tracks[k]

        #print('----------------------------------------------')
        #print(self.__track_detections)
        #print('----------------------------------------------')
        

        return track_coordinates

    def __get_new_or_lost_tracks(self):

        new_tracks = {}

        # Identify tracks of new targets and the ones which have their
        # primary trackers too far from the solutions,
        # so need to be re-initialized

        # NOTE: always len(self.__tracks) == len(self.__track_detections)
        for i, track in enumerate(self.__track_detections):

            if ((track[-1] != '' and track[-2] == '') or
                (self.__tracks[i].are_trackers_lost())):

                new_id = self.__tracks[i].get_track_id()
                new_box = self.__tracks[i].get_last_detection()
                new_tracks[new_id] = new_box

        return new_tracks

    def __get_trackers_score( self,
                             detection,
                             tracker_results):

        # Get score based on distances between 
        # new detection and primary trackers
        is_inside = []
        distances = []
        is_lost = []
                                
        for box in tracker_results:

            box_center = self.__get_center(box=box)
                                
            det_center = self.__get_center(box=detection)

            dist = self.__get_euclidean_distance( c1=box_center,
                                                  c2=det_center)
                                
            if dist < self.__d_th:

                is_inside.append(True)
                distances.append(dist)

                if dist >= self.__d_th2:

                    is_lost.append(True)

                else:

                    is_lost.append(False)
            else:
                                
                is_inside.append(False)
                distances.append(None)
                is_lost.append(True)
                                
        score = 0

        # The new observation is considered to extend a track
        # if one or more primary trackers are inside its gating area     
        if any(is_inside):
                                
            inside = True
                                
            for i, distance in enumerate(distances):

                if distance is not None:

                    # y=(1/th^2)*(x-th)^2
                    score += (1/self.__d_th**2)*((distance-self.__d_th)**2)*self.__trackers_weights[i] 
        else:
                                
            inside = False

        # If all primary trackers exceed the threshold, they are lost       
        if all(is_lost):
                                
            trackers_lost = True
                                
        else:
                                
            trackers_lost = False
                                
        return inside, score, trackers_lost

    def __get_center( self,
                    box):
                                
        # Get bboxes centers
        x1 = box[0]
        y1 = box[1]
        x2 = box[2]+box[0]
        y2 = box[3]+box[1]
        
        xc = (x1+x2)/2
        yc = (y1+y2)/2
        center = (xc, yc)
                                
        return center

    def __get_euclidean_distance( self,
                      c1,
                      c2):
                                
        # Euclidean distance
        distance = np.sqrt( np.power(c1[1]-c2[1], 2) + np.power(c1[0]-c2[0], 2))

        return distance

    def __get_conflicting_tracks( self,
                                  track_detections):

        # Identify conflicting tracks
        conflicting_tracks = []

        for i in range(len(track_detections)):

            for j in range(i + 1, len(track_detections)):

                left_ids = track_detections[i]
                right_ids = track_detections[j]

                for k in range(len(left_ids)):

                    if left_ids[k] != '' and right_ids[k] != '' and left_ids[k] == right_ids[k]:

                        conflicting_tracks.append((i, j))

        return conflicting_tracks

    def __get_global_hypothesis(self,
                               tracks,
                               conflicting_tracks):

        """
        Generate a global hypothesis by finding the maximum weighted independent
        set of a graph with tracks as vertices, and edges between conflicting tracks.
        """

        gh_graph = WeightedGraph()

        for index, track in enumerate(tracks):

            s = track.get_track_score()
                                
            gh_graph.add_weighted_vertex(str(index), s)

        gh_graph.set_edges(conflicting_tracks)

        mwis_ids = gh_graph.mwis()
        
        return mwis_ids

    def run(self,
            detections_list,
            trackers_results_dict,
            external_hypothesis_list):
                       
        self.__update_tracks( detections=detections_list,
                              trackers_results=trackers_results_dict,
                              external_hypothesis_list=external_hypothesis_list)
            
        # Conflicting tracks: share an observation at any time
        conflicting_tracks = self.__get_conflicting_tracks(self.__track_detections) 

        # MWIS
        solution_ids = self.__get_global_hypothesis(self.__tracks,
                                                    conflicting_tracks) 

        track_coordinates = self.__prune_track_tree( solution_ids=solution_ids,
                                                     detections=detections_list)

        solution_dictionary = self.__get_output_format(data_dictionary=track_coordinates)

        # Get the ID from each solution hypothesis
        track_ids = []

        for track in self.__tracks:

            track_ids.append(track.get_track_id())

        self.__traject_count = max(track_ids)+1

        new_tracks = self.__get_new_or_lost_tracks()
        
        self.__frame_index += 1

        global_hypothesis = []

        for track_d in self.__track_detections:

            if track_d[-1] != '' and track_d[-2] != '':

                global_hypothesis.append((int(track_d[-2]), int(track_d[-1])))

            elif track_d[-1] != '' and track_d[-2] == '':

                global_hypothesis.append((-1,int(track_d[-1])))

            '''elif track_d[-1] == '' and track_d[-2] != '':

                global_hypothesis.append((int(track_d[-2]),-1))'''

        return solution_dictionary, new_tracks, global_hypothesis

    def get_frame_index(self):

        return self.__frame_index