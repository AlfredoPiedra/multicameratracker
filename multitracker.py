import sys
import cv2
import time
import numpy as np
import random

from mht import MHT
from constants import DIRECTORY_PATH
from detection_reader import DetectionReader


def generate_mht_bbox_dictionary(cv2_format_bbox,
                                 targets_tracked):

    mht_format_bbox = []

    for box in cv2_format_bbox:

        mht_format_bbox.append((box[0],box[1],box[2],box[3]))
    
    bboxes_dict = {}

    for i in targets_tracked:
        
        bboxes_dict[i] = []
        bboxes_dict[i].append(mht_format_bbox.pop(0))
        bboxes_dict[i].append(mht_format_bbox.pop(0))
        bboxes_dict[i].append(mht_format_bbox.pop(0))

    return bboxes_dict

def main():

    # Initialize the input media reader
    det_reader = DetectionReader("image",
                                 DIRECTORY_PATH + "/multimedia/source_image_list.txt",
                                 DIRECTORY_PATH + "/multimedia/source_image_list.txt")
    
    # Multitracker object (primary trackers)
    multitracker = cv2.MultiTracker_create()

    # Get the first frame
    ok,frame = det_reader.get_next_frame()

    if not ok:
        
        print("[Main]: could not get the first frame")
        sys.exit()

    # Get the first frame bboxes in the opencv required format
    ok,bboxes = det_reader.get_next_bboxes()

    if not ok:

        print("[Main]: could not get the first bbox info")
        sys.exit()
        

    # Initialization of primary trackers for each target
    for bbox in bboxes:
        
        multitracker.add(cv2.TrackerKCF_create(), frame, bbox)
        multitracker.add(cv2.TrackerMIL_create(), frame, bbox)
        multitracker.add(cv2.TrackerCSRT_create(), frame, bbox)
        
    # MHT
    tracking_params = {'N_pruning': 0,            # Index for pruning
                       'distance_threshold': 100, # Distance threshold for hypothesis formation
                       'distance_threshold2': 75, # Distance threshold for the updating of primary trackers
                       'MIL_weight': 0.2,         # MIL Tracker weight on the track scoring
                       'KCF_weight': 0.35,        # MF Tracker weight on the track scoring
                       'CSRT_weight': 0.45        # KCF Tracker weight on the track scoring      
                       }

    # Object MHT initialized
    mht = MHT(tracking_params)

    print("Running MHT...")
    
    mht.init(bboxes)

    #print("#################################")
    #print("Coordinates")
    #print(mht.coordinates)
    #print("Detections")
    #print(mht.track_detections)
    #print("#################################")

    ids = []
    frame_index = 0
    targets_tracked = []

    for i in range(len(bboxes)):

        targets_tracked += [i]

    read_time = 0.0
    trackers_time = 0.0
    run_time = 0.0
    update_time = 0.0

    start = time.time()

    while True:

        #start = time.time()
        # Read the next frame
        ok_frame,frame = det_reader.get_next_frame()

        # Read the next bboxes info
        ok_bbox,bboxes = det_reader.get_next_bboxes()

        if not (ok_frame and ok_bbox):
            
            break

        #read_time += (time.time() - start)

        #start = time.time() 
        # Update the primary trackers for the current frame
        ret,multitracker_results =  multitracker.update(frame)
        #trackers_time += (time.time() - start)

        #start = time.time()

        # Turn results from primary trackers into a dictionary for MHT
        multitracker_dict = generate_mht_bbox_dictionary(multitracker_results,
                                                         targets_tracked)

        # Run MHT with annotations (detections) and tracker results
        solution_coord, track_ids, new_tracks = mht.run(bboxes,
                                                        multitracker_dict)
        #print("#################################")
        #print("Coordinates")
        #print(mht.coordinates)

        #print("Detections")
        #print(mht.track_detections)
        #print("#################################")
        #run_time += (time.time() - start)

        '''sol_count = 0
        for solution in solution_coord:
            
            if solution[-1] != None:

                    # Tracking success
                    coord = solution[-1]
                    p1 = (int(coord[0]), int(coord[1]))
                    p2 = (int(coord[0] + coord[2]), int(coord[1] + coord[3]))
                    cv2.rectangle(frame, p1, p2, (255,0,0), 2, 1)

                    cv2.putText( frame,
                                 str(sol_count),
                                 p1,
                                 cv2.FONT_HERSHEY_SIMPLEX,
                                 0.75,
                                 (50,170,50),
                                 2)

            sol_count += 1
                    
                    
        cv2.imshow("Tracking", frame)
        cv2.waitKey(0)'''
                    

        #print(len(solution_coord))

        #start = time.time()
        # Update primary trackers when they're lost or there are new targets
        if len(new_tracks) != 0:
            
            for key in new_tracks.keys():

                # If an existing tracker needs to be updated
                if key in ids: 

                    i = targets_tracked.index(key)

                    # Change ID
                    targets_tracked[i] = random.randint(50,500) 

                # Append corresponding ID
                targets_tracked.append(key) 

                # Append new trackers for the target
                new_box = new_tracks[key]
                #new_box = (box[0], box[1], box[2]-box[0], box[3]-box[1])

                multitracker.add(cv2.TrackerKCF_create(), frame, new_box)
                multitracker.add(cv2.TrackerMIL_create(), frame, new_box)
                multitracker.add(cv2.TrackerCSRT_create(), frame, new_box)

        # Update track ID's
        ids = track_ids 
        frame_index += 1

        #update_time += (time.time() - start)

    total = time.time() - start
    print("####################")
    print("Execution time: "+ str(total))

    #total_time = read_time + trackers_time + run_time + update_time
    #print("Read time: " + str(read_time))
    #print("Trackers time: " + str(trackers_time))
    #print("Run time: " + str(run_time))
    #print("Update time: " + str(update_time))
    #print("Total time: " + str(total_time))

if __name__ == '__main__':

    main()

