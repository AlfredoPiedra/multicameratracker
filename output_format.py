from sys import exit
from json import dumps
from constants import DIRECTORY_PATH,OUTPUT_FORMATS,TRACKERS_OUTPUT_DIRECTORY

class OutputFormat:

    def __init__(self,
                 output_filename,
                 format_option=None):

        # Absolute path of the output file
        self.__filename = DIRECTORY_PATH + "/" + TRACKERS_OUTPUT_DIRECTORY + "/" + output_filename

        # Used to know if before saving the data to the output file
        # a conversion of the coordinates format is needed
        self.__format_mode = format_option
        
    def __save_result_json_format(self,
                                  data_dictionary_list):

        try:

            # Open/Close the output file
            with open(self.__filename, mode="a") as outfile:

                # Save all the dictionaries as a JSON Object in the file
                for data_dictionary in data_dictionary_list:

                    # Convert the python dictionary to a JSON object
                    jsonstr = dumps(data_dictionary)
                
                    # Write the JSON object in the output file as a new line
                    outfile.write(jsonstr+"\n")

        except IOError:

            print('[OutputFormat][save_result]: could not open or write in file: ' + self.__filename)
            exit()    

    def __convert_to_yolo_format(self,
                                cv_format_dict_list):

        return cv_format_dict_list

    def save_result(self,
                    data_dictionary_list):

        # In case we want to save the detections in the default opencv format
        if self.__format_mode == None:

            # No need to change format, so save directly the dictionary as a json object
            self.__save_result_json_format(data_dictionary_list=data_dictionary_list)

        # In case we want to save the detections in the yolo bounding box coordinates format
        elif self.__format_mode == OUTPUT_FORMATS['yolo']:

            # Convert the opencv format coordinates to YOLO format coordinates
            # in all the dictionaries of the list
            yolo_format_dict_list = self.__convert_to_yolo_format(cv_format_dict_list=data_dictionary_list)
            
            # Save the dictionary with YOLO format coordinates as a JSON object
            self.__save_result_json_format(data_dictionary_list=yolo_format_dict_list)